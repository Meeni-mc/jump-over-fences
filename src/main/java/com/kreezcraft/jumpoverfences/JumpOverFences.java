package com.kreezcraft.jumpoverfences;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("jumpoverfences")
public class JumpOverFences
{
    static final Logger LOGGER = LogManager.getLogger();

    public JumpOverFences() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::init);
    }

    private void init(FMLClientSetupEvent evt) {
    }
}
