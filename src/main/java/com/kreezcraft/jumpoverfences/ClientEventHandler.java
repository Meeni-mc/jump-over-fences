package com.kreezcraft.jumpoverfences;

import net.minecraft.block.Block;
import net.minecraft.block.FenceBlock;
import net.minecraft.block.WallBlock;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(
        modid = "jumpoverfences",
        value = {Dist.CLIENT}
)
public class ClientEventHandler {
    @SubscribeEvent
    public static void onPlayerJump(LivingEvent.LivingJumpEvent event) {
        if (event.getEntity() instanceof ClientPlayerEntity) {
            ClientPlayerEntity player = (ClientPlayerEntity)event.getEntity();
            if (player.movementInput.jump && isPlayerNextToFence(player)) {
                player.setMotion(player.getMotion().add(0.0D, 0.05D, 0.0D));
            }
        }

    }

    private static boolean isPlayerNextToFence(ClientPlayerEntity player) {
        double x = player.getPosX() - 1.0D;
        double y = player.getPosY();
        double z = player.getPosZ() - 1.0D;

        for(int i = 0; i < 3; ++i) {
            for(int j = 0; j < 3; ++j) {
                if ((double)i != x || (double)j != z) {
                    Block block = getBlock(player.world, new BlockPos(x + (double)i, y, z + (double)j));
                    if (block instanceof FenceBlock || block instanceof WallBlock) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static Block getBlock(World world, BlockPos pos) {
        return world.getBlockState(pos).getBlock();
    }
}
